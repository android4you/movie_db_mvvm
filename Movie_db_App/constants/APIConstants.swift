//
//  APIConstants.swift
//  Movie_db_App
//
//  Created by Manu Aravind on 09/04/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import Foundation


struct APIConstants {
     static let BASE_URL    = "https://api.themoviedb.org/3/"
     static let IMAGE_URL   = "http://image.tmdb.org/t/p/w300/"
     static let API_KEY     = "9b1eac77c34423c36d15affaa21680f2"
}
