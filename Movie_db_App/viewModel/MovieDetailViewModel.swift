//
//  MovieDetailViewModel.swift
//  Movie_db_App
//
//  Created by Manu Aravind on 08/04/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import Foundation

protocol MovieDetailViewModelDelegate: class {
    func getResponse(dataArray: MovieDetailResponse )
    func getErrorResponse(error: String)
}

class MovieDetailViewModel {
    
    var movieDetailsRespository: MovieDetailsRespository?
    weak var delegate: MovieDetailViewModelDelegate?
    
    init() {
        movieDetailsRespository = MovieDetailsRespository()
    }
    
    func getMovieDetails(movieId: Int) {
        guard let repo = movieDetailsRespository else { return }
        repo.getMovieDetails(movie_id: movieId) { [weak self](response) in
            guard let strongSelf = self else { return }
            switch response {
            case .success(let result):
                strongSelf.delegate?.getResponse(dataArray: result)
            case.failure:
                print("Failed Appi")
                break
            }
        }
    }
    
}
