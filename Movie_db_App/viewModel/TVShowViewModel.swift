//
//  TVShowViewModel.swift
//  Movie_db_App
//
//  Created by Manu Aravind on 07/04/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import Foundation

protocol TVShowViewModelDelegate: class {
    func reloadTableMovies(dataArray: [MovieModel] )
}

class TVShowViewModel {
    
    var tvShowRepository: TvShowsRepository?
    weak var delegate: TVShowViewModelDelegate?
    var tvShowsDataItems: [MovieModel] = []
    
    init() {
        tvShowRepository = TvShowsRepository()
    }
    
    func getPopularTvShows(){
        guard let repotv = tvShowRepository else { return }
        repotv.getTvShows(type : TvShowType().popular) { [weak self](response) in
            guard let strongSelf = self else { return }
            switch response {
            case.success(let result):
                var resp : MovieListResponse = result
                resp.movieType = "Popular TvShows"
                resp.pos = 4
                strongSelf.delegate?.reloadTableMovies(dataArray: resp.results ?? [])
            case.failure:
                break
            }
        }
    }
    
    func getTopRatedTvShows(){
        guard let repotv = tvShowRepository else { return }
        repotv.getTvShows(type : TvShowType().top_rated) { [weak self](response) in
            guard let strongSelf = self else { return }
            switch response {
            case.success(let result):
                var resp : MovieListResponse = result
                resp.movieType = "TopRated TvShows"
                resp.pos = 5
                strongSelf.delegate?.reloadTableMovies(dataArray: resp.results ?? [])
            case.failure:
                break
            }
        }
    }
    
    func getOnAirTvShows(){
        guard let repotv = tvShowRepository else { return }
        repotv.getTvShows(type : TvShowType().on_the_air) { [weak self](response) in
            guard let strongSelf = self else { return }
            switch response {
            case.success(let result):
                var resp : MovieListResponse = result
                resp.movieType = "On Air TvShows"
                resp.pos = 6
                strongSelf.delegate?.reloadTableMovies(dataArray: resp.results ?? [])
            case.failure:
                break
            }
        }
    }
    
    func getAiringTvShows(){
        guard let repotv = tvShowRepository else { return }
        repotv.getTvShows(type : TvShowType().airing_today) { [weak self](response) in
            guard let strongSelf = self else { return }
            switch response {
            case.success(let result):
                var resp : MovieListResponse = result
                resp.movieType = "Airing Today TvShows"
                resp.pos = 7
                strongSelf.delegate?.reloadTableMovies(dataArray: resp.results ?? [])
                
            case.failure:
                break
            }
        }
    }
    
}
