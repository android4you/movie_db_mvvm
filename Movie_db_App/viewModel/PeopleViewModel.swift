//
//  PeopleViewModel.swift
//  Movie_db_App
//
//  Created by Manu Aravind on 08/04/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import Foundation

protocol PeopleViewModelDelegate: class {
    func reloadTablePeople(dataArray: [PeopleModel] )
}

class PeopleViewModel {
    
    var peopleRepository: PeopleRepository?
    weak var delegate: PeopleViewModelDelegate?
    
    init() {
        peopleRepository = PeopleRepository()
    }
    
    func getPopularPeople() {
        guard let repo = peopleRepository else { return }
        repo.getPopularPeople() { [weak self](response) in
            guard let strongSelf = self else { return }
            switch response {
            case .success(let result):
                var resp : PeopleListResponse = result
                resp.movieType = "Popular Movies"
                resp.pos = 0
                strongSelf.delegate?.reloadTablePeople(dataArray: resp.results ?? [])
            case.failure:
                print("Failed Appi")
                break
            }
        }
    }
    
}
