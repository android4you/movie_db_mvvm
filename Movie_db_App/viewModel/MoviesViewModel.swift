//
//  MoviesViewModel.swift
//  Movie_db_App
//
//  Created by Manu Aravind on 20/03/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import Foundation

protocol MoviesViewModelDelegate: class {
    func reloadTableMovies(dataArray: [MovieModel])
    func getErrorResponse(error: String)
}

class MoviesViewModel {
    var movieRepository: MoviesRepository?
    weak var delegate: MoviesViewModelDelegate?
    
    init() {
        movieRepository = MoviesRepository()
    }
    
    func getPopularMovies() {
        guard let repo = movieRepository else { return }
        repo.getMovies(type : MovieType().popular) { [weak self](response) in
            guard let strongSelf = self else { return }
            switch response {
            case .success(let result):
                var resp : MovieListResponse = result
                resp.movieType = "Popular Movies"
                resp.pos = 0
                strongSelf.delegate?.reloadTableMovies(dataArray: resp.results ?? [])
            case.failure:
                print("Failed Appi")
                break
            }
        }
    }
    
    func getTopRatedMovies() {
        guard let repo = movieRepository else { return }
        repo.getMovies(type : MovieType().top_rated) { [weak self](response) in
            guard let strongSelf = self else { return }
            switch response {
            case .success(let result):
                var resp : MovieListResponse = result
                resp.movieType = "TopRated Movies"
                resp.pos = 1
                strongSelf.delegate?.reloadTableMovies(dataArray: resp.results ?? [])
            case.failure:
                break
            }
        }
    }
    
    func getNowPlayingMovies() {
        guard let repo = movieRepository else { return }
        repo.getMovies(type : MovieType().now_playing) { [weak self](response) in
            guard let strongSelf = self else { return }
            switch response {
            case .success(let result):
                var resp : MovieListResponse = result
                resp.movieType = "Now Playing Movies"
                resp.pos = 2
                strongSelf.delegate?.reloadTableMovies(dataArray: resp.results ?? [])
            case.failure:
                break
            }
        }
    }
    
    func getUpcomingMovies() {
        guard let repo = movieRepository else { return }
        repo.getMovies(type : MovieType().upcoming) { [weak self](response) in
            guard let strongSelf = self else { return }
            switch response {
            case .success(let result):
                var resp : MovieListResponse = result
                resp.movieType = "Upcoming Movies"
                resp.pos = 3
                strongSelf.delegate?.reloadTableMovies(dataArray: resp.results ?? [])
            case.failure:
                break
            }
        }
    }
}
