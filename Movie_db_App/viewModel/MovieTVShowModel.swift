//
//  MovieTVShowModel.swift
//  Movie_db_App
//
//  Created by Manu Aravind on 24/03/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import Foundation

protocol MovieTVShowModelDelegate: class {
    func reloadTableMovies(dataArray: [AnyObject] )
    func reloadTableTvshows(dataArray: [AnyObject] )
}

class MovieTVShowModel {
    var movieRepository: MoviesRepository?
    var tvShowRepository: TvShowsRepository?
    weak var delegate: MovieTVShowModelDelegate?
    
    var moviesDataItems: [AnyObject] = []
    var tvShowsDataItems: [AnyObject] = []
    
    init() {
        movieRepository = MoviesRepository()
        tvShowRepository = TvShowsRepository()
    }
  
    func getMovies() {
        self.getPopularMovies()
    }
    
    func getPopularMovies() {
        guard let repo = movieRepository else { return }
        repo.getMovies(type : MovieType().popular) { [weak self](response) in
            guard let strongSelf = self else { return }
            switch response {
            case .success(let result):
                var resp : MovieListResponse = result
                resp.movieType = "Popular Movies"
                resp.pos = 0
                strongSelf.moviesDataItems.insert((resp as? AnyObject)!, at: 0)
                self?.getTopRatedMovies()
            case.failure:
                break
            }
        }
    }
    
    func getTopRatedMovies() {
        guard let repo = movieRepository else { return }
        repo.getMovies(type : MovieType().top_rated) { [weak self](response) in
            guard let strongSelf = self else { return }
            switch response {
            case .success(let result):
                var resp : MovieListResponse = result
                resp.movieType = "TopRated Movies"
                resp.pos = 1
                strongSelf.moviesDataItems.insert((resp as? AnyObject)!, at: 1)
                self?.getNowPlayingMovies()
            case.failure:
                break
            }
        }
    }
    
    func getNowPlayingMovies() {
        guard let repo = movieRepository else { return }
        repo.getMovies(type : MovieType().now_playing) { [weak self](response) in
            guard let strongSelf = self else { return }
            switch response {
            case .success(let result):
                var resp : MovieListResponse = result
                resp.movieType = "Now Playing Movies"
                resp.pos = 2
                strongSelf.moviesDataItems.insert((resp as? AnyObject)!, at: 2)
                self?.getUpcomingMovies()
            case.failure:
                break
            }
        }
    }
    
    func getUpcomingMovies() {
        guard let repo = movieRepository else { return }
        repo.getMovies(type : MovieType().upcoming) { [weak self](response) in
            guard let strongSelf = self else { return }
            switch response {
            case .success(let result):
                var resp : MovieListResponse = result
                resp.movieType = "Upcoming Movies"
                resp.pos = 3
                strongSelf.moviesDataItems.insert((resp as? AnyObject)!, at: 3)
                self?.getPopularTvShows()
            case.failure:
                break
            }
        }
    }

    func getPopularTvShows(){
        guard let repotv = tvShowRepository else { return }
        repotv.getTvShows(type : TvShowType().popular) { [weak self](response) in
            guard let strongSelf = self else { return }
            switch response {
            case.success(let result):
                var resp : MovieListResponse = result
                resp.movieType = "Popular TvShows"
                resp.pos = 4
                strongSelf.moviesDataItems.insert((resp as? AnyObject)!, at: 4)
                self?.getTopRatedTvShows()
            case.failure:
                break
            }
        }
    }
    
    func getTopRatedTvShows(){
        guard let repotv = tvShowRepository else { return }
        repotv.getTvShows(type : TvShowType().top_rated) { [weak self](response) in
            guard let strongSelf = self else { return }
            switch response {
            case.success(let result):
                var resp : MovieListResponse = result
                resp.movieType = "TopRated TvShows"
                resp.pos = 5
                strongSelf.moviesDataItems.insert((resp as? AnyObject)!, at: 5)
                self?.getOnAirTvShows()
            case.failure:
                break
            }
        }
    }
    
    func getOnAirTvShows(){
        guard let repotv = tvShowRepository else { return }
        repotv.getTvShows(type : TvShowType().on_the_air) { [weak self](response) in
            guard let strongSelf = self else { return }
            switch response {
            case.success(let result):
                var resp : MovieListResponse = result
                resp.movieType = "On Air TvShows"
                resp.pos = 6
                strongSelf.moviesDataItems.insert((resp as? AnyObject)!, at: 6)
                self?.getAiringTvShows()
            case.failure:
                break
            }
        }
    }
    
    func getAiringTvShows(){
        guard let repotv = tvShowRepository else { return }
        repotv.getTvShows(type : TvShowType().airing_today) { [weak self](response) in
            guard let strongSelf = self else { return }
            switch response {
            case.success(let result):
                var resp : MovieListResponse = result
                resp.movieType = "Airing Today TvShows"
                resp.pos = 7
                strongSelf.moviesDataItems.insert((resp as? AnyObject)!, at: 7)
                strongSelf.delegate?.reloadTableTvshows(dataArray: self?.moviesDataItems ?? [] )
            case.failure:
                break
            }
        }
    }
    
    
}
