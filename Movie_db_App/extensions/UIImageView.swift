//
//  UIImageView.swift
//  Movie_db_App
//
//  Created by Manu Aravind on 20/03/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import Foundation
import UIKit

extension UIImageView {


    func setImageFromURl(stringImageUrl url: String){
        if let url = NSURL(string: url) {
            if let data = NSData(contentsOf: url as URL) {
                DispatchQueue.main.async {
                    self.image = UIImage(data: data as Data)
                }
            }
        }
    }



}
