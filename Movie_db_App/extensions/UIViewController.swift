//
//  UIViewController.swift
//  Movie_db_App
//
//  Created by Manu Aravind on 19/03/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import UIKit

extension UIViewController
{
    func setNaviationBar(headLine: String){
        self.navigationItem.title = headLine
        self.navigationController?.navigationBar.barTintColor = UIColor.init(hex: "be2b00")
        self.navigationController?.navigationBar.titleTextAttributes = [
            //NSAttributedString.Key.font: UIFont(name: "Futura-Medium", size: 16)!,
            NSAttributedString.Key.foregroundColor: UIColor.white
        ]
    }
    
    
    func hideKeyboard()
    {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(UIViewController.dismissKeyboard))
        
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard()
    {
        view.endEditing(true)
    }
    
    func setNavigationBarItem() {
      //  self.navigationController?.navigationBar.backItem?.title = ""
        
    }
    
    @objc func backButtonAction() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func addBackbutton(title: String) {
        if let nav = self.navigationController,
            let item = nav.navigationBar.topItem {
            item.backBarButtonItem  = UIBarButtonItem(title: title, style: UIBarButtonItem.Style.plain, target: self, action:
                #selector(self.backButtonAction))
        } else {
            if let nav = self.navigationController,
                let _ = nav.navigationBar.backItem {
                self.navigationController!.navigationBar.backItem!.title = title
            }
        }
    }
    
}
