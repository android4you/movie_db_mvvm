//
//  UIApplication.swift
//  Movie_db_App
//
//  Created by Manu Aravind on 19/03/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import UIKit

extension UIApplication {
    var keyWindowInConnectedScenes: UIWindow? {
          return windows.first(where: { $0.isKeyWindow })
    }
}
