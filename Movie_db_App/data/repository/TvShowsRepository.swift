//
//  TvShowsRepository.swift
//  Movie_db_App
//
//  Created by Manu Aravind on 20/03/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import Foundation

class TvShowsRepository {
    
    func getTvShows(type: String, completion: @escaping (ItemDataTVResponse) -> Void ) {
        let url = URL(string: APIConstants.BASE_URL +  "tv/" + type + "?api_key=" + APIConstants.API_KEY )
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "GET"
        URLSession.shared.dataTask(with: url!, completionHandler: {
            (data, response, error) in
            do{
                if error != nil{
                    print("error")
                    completion(.failure)
                    return
                }
                if let res = response as?  HTTPURLResponse, res.statusCode == 200 {
                    let movie = try! JSONDecoder().decode(MovieListResponse.self, from: data!)
                    DispatchQueue.main.async {
                        completion(.success(result: movie))
                    }
                }else{
                    print(response!.description)
                    completion(.failure)
                    return
                }
            }
            catch{
                completion(.failure)
                print("Exception thrown")
            }
            
        }).resume()
    }
}
