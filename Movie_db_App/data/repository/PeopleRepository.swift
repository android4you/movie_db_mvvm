//
//  PeopleRepository.swift
//  Movie_db_App
//
//  Created by Manu Aravind on 08/04/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import Foundation

class PeopleRepository  {
    
    func getPopularPeople(completion: @escaping (ItemDataPeopleResponse) -> Void ) {
        let url = URL(string: APIConstants.BASE_URL +  "person/popular?api_key=" + APIConstants.API_KEY )
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "GET"
        URLSession.shared.dataTask(with: url!, completionHandler: {
            (data, response, error) in
            do{
                if error != nil{
                    print("error")
                    completion(.failure)
                    return
                }
                
                // print("resoponse : \((response as?  HTTPURLResponse)?.statusCode)")
                if let res = response as?  HTTPURLResponse, res.statusCode == 200 {
                    let peoples = try! JSONDecoder().decode(PeopleListResponse.self, from: data!)
                    DispatchQueue.main.async {
                        completion(.success(result: peoples))
                    }
                }else{
                    print(response!.description)
                    completion(.failure)
                    return
                }
            }
            catch{
                completion(.failure)
                print("Exception thrown")
            }
            
        }).resume()
    }
}
