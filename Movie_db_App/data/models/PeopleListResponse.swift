//
//  PeopleListResponse.swift
//  Movie_db_App
//
//  Created by Manu Aravind on 08/04/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import Foundation



struct PeopleListResponse: Codable {
    
    var movieType      : String?
    
    var pos            : Int?
    
    let total_pages    : Int?
   
    let total_results  : Int?
   
   var results        : [PeopleModel]?
    
}
