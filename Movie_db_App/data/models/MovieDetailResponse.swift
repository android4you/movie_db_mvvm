//
//  MovieDetailResponse.swift
//  Movie_db_App
//
//  Created by Manu Aravind on 08/04/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import Foundation

struct MovieDetailResponse: Codable {
    

//      let adult: Bool?
//
//      let backdrop_path: String?
//
      let budget: Int?
//
     let homepage: String?
//
//      let id:  Int?
//
//      let original_language: String?

      let original_title: String?

      let overview: String?

//      let popularity: Double?
//
//      let poster_path: String?
//
     let release_date: String?
//
     let revenue: Int?
//
//      var status: String?
//
//      var tagline: String?
//
//      var title: String?
//
//      var video: Bool?
//
     var vote_average:  Double?
//
     var vote_count : Int?

    
}
