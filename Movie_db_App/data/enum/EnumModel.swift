//
//  EnumModel.swift
//  Movie_db_App
//
//  Created by Manu Aravind on 20/03/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import Foundation


enum  ItemDataResponse {
    case success(result: MovieListResponse)
    case failure
}

enum  ItemDataTVResponse {
    case success(result: MovieListResponse)
    case failure
}

enum  ItemDataPeopleResponse {
    case success(result: PeopleListResponse)
    case failure
}

enum  ItemDataMovieDetailsResponse {
    case success(result: MovieDetailResponse)
    case failure
}



enum ServiceResponse {
    case success(response: MovieListResponse)
    case failure
    case notConnectedToInternet
}


enum ServiceTVResponse {
    case success(response: MovieListResponse)
    case failure
    case notConnectedToInternet
}


enum ResponseStatusCode: Int {
    case success = 200
}


struct MovieType {

    var upcoming:String  = "upcoming";
    var popular: String  = "popular";
    var top_rated:String  = "top_rated";
    var now_playing: String  = "now_playing";


}


struct TvShowType {

    var on_the_air:String  = "on_the_air";
    var popular: String  = "popular";
    var top_rated:String  = "top_rated";
    var airing_today: String  = "airing_today";


}
