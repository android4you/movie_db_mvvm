//
//  TabItemView.swift
//  Movie_db_App
//
//  Created by Manu Aravind on 07/04/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import UIKit

@IBDesignable
class TabItemView: UIButton {

    override func awakeFromNib() {
        super.awakeFromNib()
        setUpButton()
    }
    
    func setUpButton()  {
        backgroundColor = UIColor.white  // UIColor.init(hex: "be2b00")
    }
}

