//
//  HomeTableViewCell.swift
//  Movie_db_App
//
//  Created by Manu Aravind on 19/03/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import UIKit

protocol ItemSelectListener {
    func selectedItem(movie: MovieModel?)
}


class HomeTableViewCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    let cellId = "MovieCollectionViewCell"
    
    @IBOutlet weak var categoryTitle: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var dataResults : [AnyObject]?
    var delegate : ItemSelectListener?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        collectionView.register(UINib.init(nibName: cellId, bundle: nil), forCellWithReuseIdentifier: cellId)
//        collectionView.translatesAutoresizingMaskIntoConstraints = false;
//        collectionView.layoutIfNeeded()
        //  collectionView.reloadData()
    }
    
    func updateCellWith(row: [AnyObject]) {
        self.dataResults = row
        self.collectionView.reloadData()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataResults?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! MovieCollectionViewCell
        let movie = dataResults?[indexPath.row] as? MovieModel
        
        if(movie?.title != nil){
            let url = APIConstants.IMAGE_URL + (movie?.poster_path ?? "")
            cell.movieTitle.text = movie?.title
            cell.movieImage.setImageFromURl(stringImageUrl: url)
        }else {
            let url = APIConstants.IMAGE_URL + (movie?.poster_path ?? "")
            cell.movieTitle.text = movie?.name
            cell.movieImage.setImageFromURl(stringImageUrl: url)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.bounds.size.width/3, height: 200)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let movie = dataResults?[indexPath.row] as? MovieModel
        if(movie?.title != nil){
            print(movie?.title)
            delegate?.selectedItem(movie: movie ?? nil)
            
        }else {
            
        }
        
    }
}

