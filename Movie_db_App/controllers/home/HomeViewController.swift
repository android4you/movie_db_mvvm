//
//  HomeViewController.swift
//  Movie_db_App
//
//  Created by Manu Aravind on 19/03/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import UIKit

class HomeViewController: BaseViewController, MovieTVShowModelDelegate, ItemSelectListener {
    
    var progressUtils = ViewControllerUtils()
    let cellId = "HomeTableViewCell"
    var viewModel = MovieTVShowModel()
    var dataItems : [AnyObject] = []
    
    @IBOutlet weak var homeTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        setNaviationBar(headLine: "Home")
        progressUtils.showActivityIndicator(uiView: view)
        homeTableView.register(UINib.init(nibName: cellId, bundle: nil), forCellReuseIdentifier: cellId)
        homeTableView.separatorColor = UIColor.clear
        homeTableView.translatesAutoresizingMaskIntoConstraints = false;
        homeTableView.rowHeight = 300
        homeTableView.layoutIfNeeded()
        viewModel.delegate = self
        viewModel.getMovies()
    }
    
    func reloadTableMovies(dataArray: [AnyObject]) {
        self.dataItems.append(contentsOf: dataArray)
    }
    
    func reloadTableTvshows(dataArray: [AnyObject]) {
        self.dataItems.append(contentsOf: dataArray)
        progressUtils.hideActivityIndicator(uiView: view)
        homeTableView.reloadData()
    }
    
    func selectedItem(movie: MovieModel?) {
        let detailsVC = MovieDetailsViewController(nibName: "MovieDetailsViewController", bundle: nil)
        detailsVC.movieModel =  movie
        self.navigationController?.pushViewController(detailsVC, animated: true)
    }
}

extension HomeViewController: UITableViewDelegate, UITableViewDataSource   {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        dataItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! HomeTableViewCell
        cell.selectionStyle = .none
        
        if let movie = dataItems[indexPath.row] as? MovieListResponse {
            cell.categoryTitle.text = movie.movieType
            cell.updateCellWith(row: movie.results! as [AnyObject])
            cell.delegate = self
            
        }
        
        return cell
    }
}
