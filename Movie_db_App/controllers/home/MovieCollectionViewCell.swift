//
//  MovieCollectionViewCell.swift
//  Movie_db_App
//
//  Created by Manu Aravind on 19/03/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import UIKit

class MovieCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var movieTitle: UILabel!
    
    @IBOutlet weak var movieImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

}
