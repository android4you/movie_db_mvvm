//
//  MovieCell.swift
//  Movie_db_App
//
//  Created by Manu Aravind on 07/04/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import UIKit

class MovieCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
  //  @IBOutlet weak var titleView: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    func updateCell(url: String , title: String){
        imageView.setImageFromURl(stringImageUrl: url)
       // titleView.text = title
    }

}
