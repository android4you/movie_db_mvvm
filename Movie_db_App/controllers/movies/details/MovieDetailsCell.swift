//
//  MovieDetailsCell.swift
//  Movie_db_App
//
//  Created by Manu Aravind on 08/04/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import UIKit

class MovieDetailsCell: UITableViewCell {
    
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var overviewLabel: UILabel!
    
    @IBOutlet weak var release_dateLabel: UILabel!
    
    
    @IBOutlet weak var voteCountLabel: UILabel!
    
    @IBOutlet weak var voteAverageLabel: UILabel!
    
    
    @IBOutlet weak var budgetLabel: UILabel!
    
    @IBOutlet weak var revenueLabel: UILabel!
    
    @IBOutlet weak var homePageLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func updateCell(dataModel: MovieDetailResponse){
        titleLabel.text = dataModel.original_title
        overviewLabel.text = dataModel.overview
        release_dateLabel.text = dataModel.release_date
        voteCountLabel.text = String(dataModel.vote_count ?? 0)
        voteAverageLabel.text = String(dataModel.vote_average ?? 0)
        budgetLabel.text = String(dataModel.budget ?? 0)
        revenueLabel.text = String(dataModel.revenue ?? 0)
        homePageLabel.text = dataModel.homepage ?? "--"
    }
    
    
    
    
    //
    //    func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat{
    //        let label:UILabel = UILabel(frame: CGRectMake(0, 0, width, CGFloat.greatestFiniteMagnitude))
    //        label.numberOfLines = 0
    //        label.lineBreakMode = NSLineBreakMode.byWordWrapping
    //        label.font = font
    //        label.text = text
    //
    //        label.sizeToFit()
    //        return label.frame.height
    //    }
    //
}
