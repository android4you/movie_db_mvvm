//
//  MovieDetailsViewController.swift
//  Movie_db_App
//
//  Created by Manu Aravind on 08/04/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import UIKit

class MovieDetailsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, MovieDetailViewModelDelegate {
    
    
    @IBOutlet weak var movieDetailScrollView: UITableView!
    @IBOutlet weak var detailsImageView: UIImageView!
    var movieModel: MovieModel?
    let cellId = "MovieDetailsCell"
    var viewModel = MovieDetailViewModel()
    var movieDetails : MovieDetailResponse?
    var progressUtils = ViewControllerUtils()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNaviationBar(headLine: "Movie Details")
        let url = APIConstants.IMAGE_URL + (movieModel?.backdrop_path ?? "")
        detailsImageView.setImageFromURl(stringImageUrl: url)
        movieDetailScrollView.delegate = self
        movieDetailScrollView.dataSource = self
        viewModel.delegate = self
        movieDetailScrollView.register(UINib.init(nibName: cellId, bundle: nil), forCellReuseIdentifier: cellId)
        progressUtils.showActivityIndicator(uiView: self.view)
        viewModel.getMovieDetails(movieId: movieModel?.id ?? -1)
    }
    
    func getResponse(dataArray: MovieDetailResponse) {
        movieDetails = dataArray
        movieDetailScrollView.reloadData()
        progressUtils.hideActivityIndicator(uiView: self.view)
    }
    
    func getErrorResponse(error: String){
        progressUtils.hideActivityIndicator(uiView: self.view)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! MovieDetailsCell
        cell.selectionStyle = .none
        
        if let constantName = movieDetails {
            cell.updateCell(dataModel: constantName)
        }
        cell.overviewLabel.numberOfLines = 0
        cell.overviewLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        cell.overviewLabel.sizeToFit()
        return cell
    }
    
    
}
