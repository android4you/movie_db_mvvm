//
//  MovieViewController.swift
//  Movie_db_App
//
//  Created by Manu Aravind on 21/03/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import UIKit

class MovieViewController: UIViewController, MoviesViewModelDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout  {
    
    @IBOutlet weak var popularTab: TabItemView!
    @IBOutlet weak var topratedTab: TabItemView!
    @IBOutlet weak var nowplayingTab: TabItemView!
    @IBOutlet weak var upcomingTab: TabItemView!
    
    @IBOutlet weak var tabBottomBar: UIView!
    var tabIndicator : UIView!
    
    @IBOutlet weak var moviesCollectionView: UICollectionView!
    
    var viewModel = MoviesViewModel()
    var dataItems : [MovieModel] = []
    let cellId = "MovieCell"
    
    var progressUtils = ViewControllerUtils()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNaviationBar(headLine: "Movies")
        tabIndicator = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width/4, height: 3))
        tabIndicator?.backgroundColor = UIColor.red
        if tabIndicator != nil {
            tabBottomBar.addSubview(tabIndicator)
            animation(viewAnimation: tabIndicator, times: 0)
        }
        
        initializeTab()
        viewModel.delegate = self
        moviesCollectionView.delegate = self
        moviesCollectionView.dataSource = self
        moviesCollectionView.register(UINib.init(nibName: cellId, bundle: nil), forCellWithReuseIdentifier: cellId)
        moviesCollectionView.translatesAutoresizingMaskIntoConstraints = false;
        moviesCollectionView.layoutIfNeeded()
        
        viewModel.getPopularMovies()
    }
    
    func initializeTab() {
        progressUtils.showActivityIndicator(uiView: view)
        tabBottomBar.backgroundColor = UIColor.white
        popularTab.setTitleColor(UIColor.red, for: .normal)
        topratedTab.setTitleColor(UIColor.darkGray, for: .normal)
        nowplayingTab.setTitleColor(UIColor.darkGray, for: .normal)
        upcomingTab.setTitleColor(UIColor.darkGray, for: .normal)
    }
    
    
    @IBAction func popularClick(_ sender: Any) {
        progressUtils.showActivityIndicator(uiView: view)
        popularTab.setTitleColor(UIColor.red, for: .normal)
        topratedTab.setTitleColor(UIColor.darkGray, for: .normal)
        nowplayingTab.setTitleColor(UIColor.darkGray, for: .normal)
        upcomingTab.setTitleColor(UIColor.darkGray, for: .normal)
        animation(viewAnimation: tabIndicator, times: 0)
        viewModel.getPopularMovies()
        
    }
    
    @IBAction func topratedClick(_ sender: Any) {
        progressUtils.showActivityIndicator(uiView: view)
        popularTab.setTitleColor(UIColor.darkGray, for: .normal)
        topratedTab.setTitleColor(UIColor.red, for: .normal)
        nowplayingTab.setTitleColor(UIColor.darkGray, for: .normal)
        upcomingTab.setTitleColor(UIColor.darkGray, for: .normal)
        animation(viewAnimation: tabIndicator, times: 1)
        viewModel.getTopRatedMovies()
        
    }
    
    @IBAction func nowplayingClick(_ sender: Any) {
        progressUtils.showActivityIndicator(uiView: view)
        popularTab.setTitleColor(UIColor.darkGray, for: .normal)
        topratedTab.setTitleColor(UIColor.darkGray, for: .normal)
        nowplayingTab.setTitleColor(UIColor.red, for: .normal)
        upcomingTab.setTitleColor(UIColor.darkGray, for: .normal)
        animation(viewAnimation: tabIndicator, times: 2)
        viewModel.getNowPlayingMovies()
    }
    
    @IBAction func upcomingClick(_ sender: Any) {
        progressUtils.showActivityIndicator(uiView: view)
        popularTab.setTitleColor(UIColor.darkGray, for: .normal)
        topratedTab.setTitleColor(UIColor.darkGray, for: .normal)
        nowplayingTab.setTitleColor(UIColor.darkGray, for: .normal)
        upcomingTab.setTitleColor(UIColor.red, for: .normal)
        animation(viewAnimation: tabIndicator, times: 3)
        viewModel.getUpcomingMovies()
    }
    
    private func animation(viewAnimation: UIView, times: CGFloat) {
        UIView.animate(withDuration: 0.5, animations: {
            viewAnimation.frame.origin.x = viewAnimation.frame.width * times
        }) { (_) in
            
        }
    }
    
    func reloadTableMovies(dataArray: [MovieModel]) {
        self.dataItems.removeAll()
        self.dataItems.append(contentsOf: dataArray)
        print("Success")
        progressUtils.hideActivityIndicator(uiView: view)
        moviesCollectionView.reloadData()
    }
    
    func getErrorResponse(error: String) {
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! MovieCell
        let url = APIConstants.IMAGE_URL + (dataItems[indexPath.row].poster_path ?? "")
        cell.updateCell(url: url, title: dataItems[indexPath.row].title ?? "--")
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.bounds.size.width/3)-1, height: 200)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let detailsVC = MovieDetailsViewController(nibName: "MovieDetailsViewController", bundle: nil)
        detailsVC.movieModel =  dataItems[indexPath.row]
        navigationController?.pushViewController(detailsVC, animated: true)
    }
    
    
    
    
    
    
    
    
    /******
     private func setupUI(){
     // view
     view.backgroundColor = .red
     view.addSubview(slidingTabController.view) // add slidingTabController to main view
     
     // navigation
     navigationItem.title = "Sliding Tab Example"
     navigationController?.navigationBar.barTintColor = .orange
     navigationController?.navigationBar.isTranslucent = false
     navigationController?.navigationBar.shadowImage = UIImage()
     navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
     navigationController?.navigationBar.barStyle = .black
     
     // MARK: slidingTabController
     slidingTabController.addItem(item: ItemViewController(), title: "First") // add first item
     slidingTabController.addItem(item: ItemViewController(), title: "Second") // add second item
     slidingTabController.addItem(item: ItemViewController(), title: "Third") // add other item
     slidingTabController.addItem(item: ItemViewController(), title: "Forth") // add other item
     //        slidingTabController.addItem(item: ItemViewController(), title: "Fifth") // add other item
     //        slidingTabController.addItem(item: ItemViewController(), title: "Sixth") // add other item
     //        slidingTabController.addItem(item: ItemViewController(), title: "Seventh") // add other item
     slidingTabController.setHeaderActiveColor(color: .white) // default blue
     slidingTabController.setHeaderInActiveColor(color: .lightText) // default gray
     slidingTabController.setHeaderBackgroundColor(color: .orange) // default white
     slidingTabController.setCurrentPosition(position: 1) // default 0
     slidingTabController.setStyle(style: .fixed) // default fixed
     slidingTabController.build() // build
     }
     *****/
    
}
