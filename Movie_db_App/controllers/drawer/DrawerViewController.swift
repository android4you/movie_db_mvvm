//
//  DrawerViewController.swift
//  Movie_db_App
//
//  Created by Manu Aravind on 19/03/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import UIKit


protocol SlideMenuDelegate {
    func slideMenuItemSelectedAtIndex(_ index : Int32)
}

class DrawerViewController: UIViewController {
    
    var delegate : SlideMenuDelegate?
    var btnMenu : UIButton!
    
    @IBOutlet weak var drawerView: UIView!
    @IBOutlet weak var btnCloseMenu: UIButton!
    @IBOutlet weak var menuTableView: UITableView!
    let cellId = "DrawerCell"
    var models = [MenuModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UINavigationBar.appearance().barTintColor = UIColor(named: "4c1d12")
        UINavigationBar.appearance().tintColor = UIColor.white
        self.view.frame = CGRect(x: -self.view.bounds.size.width, y: 0, width: self.view.bounds.size.width,height: self.view.bounds.size.height)
        self.view.layoutIfNeeded()
        self.view.backgroundColor = UIColor.clear
        self.view.insetsLayoutMarginsFromSafeArea = false
        self.btnCloseMenu.alpha = 0.0
        self.btnCloseMenu.backgroundColor = UIColor.darkGray.withAlphaComponent(0.0)
        drawerView.backgroundColor = UIColor.init(hex: "be2b00")
        
        
        menuTableView.register(UINib.init(nibName: cellId, bundle: nil), forCellReuseIdentifier: cellId)
        menuTableView.separatorColor = UIColor.clear
        menuTableView.layoutIfNeeded()
        models = menuItems()
        menuTableView.reloadData()
        
    }
    
    func animateWhenViewAppear(){
        self.btnCloseMenu.alpha = 0.0
        self.btnCloseMenu.backgroundColor = UIColor.darkGray.withAlphaComponent(0.0)
        self.view.backgroundColor = UIColor.clear
        UIView.animate(withDuration: 0.4, animations: { () -> Void in
            self.view.frame = CGRect(x: 0, y: 0, width: self.view.bounds.size.width,height: self.view.bounds.size.height)
            self.view.layoutIfNeeded()
        }, completion: { (finished) -> Void in
            self.btnCloseMenu.backgroundColor = UIColor.darkGray.withAlphaComponent(0.4)
            self.btnCloseMenu.alpha = 0.3
        })
    }
    
    func animateWhenViewDisappear(){
        self.btnCloseMenu.alpha = 0.0
        self.view.backgroundColor = UIColor.clear
        self.btnCloseMenu.backgroundColor = UIColor.darkGray.withAlphaComponent(0.0)
        UIView.animate(withDuration: 0.4, animations: { () -> Void in
            self.view.frame = CGRect(x: -self.view.bounds.size.width, y: 0, width: self.view.bounds.size.width,height: self.view.bounds.size.height)
            self.view.layoutIfNeeded()
        }, completion: { (finished) -> Void in
            self.willMove(toParent: nil)
            self.view.removeFromSuperview()
            self.removeFromParent()
            self.btnMenu.tag = 0
            self.view.backgroundColor = UIColor.clear
            self.btnCloseMenu.alpha = 0.0
            self.btnCloseMenu.backgroundColor = UIColor.darkGray.withAlphaComponent(0.0)
        })
    }
    
    @IBAction func sidemenuCloseClick(_ sender: Any) {
        animateWhenViewDisappear()
    }
    
    func  menuItems() -> [MenuModel] {
        var models = [MenuModel]()
        models.append(MenuModel(id: 1, title: "Home", description: "Manage your Going Wallet", image: "wallet.png"))
        models.append(MenuModel(id: 2, title: "Movies", description: "Follow up on orders and important updates", image: "notification.png"))
        models.append(MenuModel(id: 3, title: "Tv Shows", description: "Update Photo Info, Change Photo..", image: "profile"))
        models.append(MenuModel(id: 4, title: "People", description: "Update delivery address", image: "map.png"))
        models.append(MenuModel(id: 5, title: "Settings", description: "FAQs, Contact support team", image: "support.png"))
        return models
    }
    
}

extension DrawerViewController : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return models.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! DrawerCell
        cell.selectionStyle = .none
        let menu = models[indexPath.row]
        cell.menuTitle.text = menu.title
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        animateWhenViewDisappear()
        if (self.delegate != nil) {
            self.delegate?.slideMenuItemSelectedAtIndex(Int32(indexPath.row))
        }
        
        
    }
}
