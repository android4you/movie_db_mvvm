//
//  TvShowsViewController.swift
//  Movie_db_App
//
//  Created by Manu Aravind on 21/03/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import UIKit

class TvShowsViewController: UIViewController, TVShowViewModelDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout  {
    
    @IBOutlet weak var progressTab: UIButton!
    @IBOutlet weak var topratedTab: UIButton!
    @IBOutlet weak var onAirTab: UIButton!
    @IBOutlet weak var airingTab: UIButton!
    
    @IBOutlet weak var tabBottomView: UIView!
    
    var viewModel = TVShowViewModel()
    var dataItems : [MovieModel] = []
    
    var progressUtils = ViewControllerUtils()
    var tabIndicator : UIView!
    
    @IBOutlet weak var tvShowCollectionView: UICollectionView!
    let cellId = "TVShowCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNaviationBar(headLine: "TV Shows")
        tabIndicator = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width/4, height: 3))
        tabIndicator?.backgroundColor = UIColor.red
        if tabIndicator != nil {
            tabBottomView.addSubview(tabIndicator)
            animation(viewAnimation: tabIndicator, times: 0)
        }
        initializeUI()
        viewModel.delegate = self
        tvShowCollectionView.delegate = self
        tvShowCollectionView.dataSource = self
        tvShowCollectionView.register(UINib.init(nibName: cellId, bundle: nil), forCellWithReuseIdentifier: cellId)
        tvShowCollectionView.translatesAutoresizingMaskIntoConstraints = false;
        tvShowCollectionView.layoutIfNeeded()
        viewModel.getPopularTvShows()
    }
    
    func initializeUI() {
        progressUtils.showActivityIndicator(uiView: view)
        tabBottomView.backgroundColor = UIColor.white
        progressTab.setTitleColor(UIColor.red, for: .normal)
        topratedTab.setTitleColor(UIColor.darkGray, for: .normal)
        onAirTab.setTitleColor(UIColor.darkGray, for: .normal)
        airingTab.setTitleColor(UIColor.darkGray, for: .normal)
    }
    
    @IBAction func progreesClick(_ sender: Any) {
        progressUtils.showActivityIndicator(uiView: view)
        progressTab.setTitleColor(UIColor.red, for: .normal)
        topratedTab.setTitleColor(UIColor.darkGray, for: .normal)
        onAirTab.setTitleColor(UIColor.darkGray, for: .normal)
        airingTab.setTitleColor(UIColor.darkGray, for: .normal)
        animation(viewAnimation: tabIndicator, times: 0)
        viewModel.getPopularTvShows()
    }
    
    @IBAction func topratedClick(_ sender: Any) {
        progressUtils.showActivityIndicator(uiView: view)
        progressTab.setTitleColor(UIColor.darkGray, for: .normal)
        topratedTab.setTitleColor(UIColor.red, for: .normal)
        onAirTab.setTitleColor(UIColor.darkGray, for: .normal)
        airingTab.setTitleColor(UIColor.darkGray, for: .normal)
        animation(viewAnimation: tabIndicator, times: 1)
        viewModel.getTopRatedTvShows()
    }
    
    @IBAction func onAirClick(_ sender: Any) {
        progressUtils.showActivityIndicator(uiView: view)
        progressTab.setTitleColor(UIColor.darkGray, for: .normal)
        topratedTab.setTitleColor(UIColor.darkGray, for: .normal)
        onAirTab.setTitleColor(UIColor.red, for: .normal)
        airingTab.setTitleColor(UIColor.darkGray, for: .normal)
        animation(viewAnimation: tabIndicator, times: 2)
        viewModel.getOnAirTvShows()
    }
    
    @IBAction func airingClick(_ sender: Any) {
        progressUtils.showActivityIndicator(uiView: view)
        progressTab.setTitleColor(UIColor.darkGray, for: .normal)
        topratedTab.setTitleColor(UIColor.darkGray, for: .normal)
        onAirTab.setTitleColor(UIColor.darkGray, for: .normal)
        airingTab.setTitleColor(UIColor.red, for: .normal)
        animation(viewAnimation: tabIndicator, times: 3)
        viewModel.getAiringTvShows()
    }
    
    private func animation(viewAnimation: UIView, times: CGFloat) {
        UIView.animate(withDuration: 0.5, animations: {
            viewAnimation.frame.origin.x = viewAnimation.frame.width * times
        }) { (_) in
            
        }
    }
    
    func reloadTableMovies(dataArray: [MovieModel]) {
        self.dataItems.removeAll()
        self.dataItems.append(contentsOf: dataArray)
        print("Success")
        progressUtils.hideActivityIndicator(uiView: view)
        tvShowCollectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! TVShowCell
        let url = APIConstants.IMAGE_URL + (dataItems[indexPath.row].poster_path ?? "")
        cell.updateCell(url: url, title: dataItems[indexPath.row].name ?? "--")
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.bounds.size.width/3)-1, height: 200)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
}
