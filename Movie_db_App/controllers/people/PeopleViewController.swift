//
//  PeopleViewController.swift
//  Movie_db_App
//
//  Created by Manu Aravind on 23/03/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import UIKit

class PeopleViewController: UIViewController, PeopleViewModelDelegate,
UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    
    
    @IBOutlet weak var peopleCollectionView: UICollectionView!
    
    var viewModel = PeopleViewModel()
    var dataItems : [PeopleModel] = []
    
    var progressUtils = ViewControllerUtils()
    let cellId = "PeopleCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNaviationBar(headLine: "People")
        
        
        viewModel.delegate = self
        peopleCollectionView.delegate = self
        peopleCollectionView.dataSource = self
        peopleCollectionView.register(UINib.init(nibName: cellId, bundle: nil), forCellWithReuseIdentifier: cellId)
        peopleCollectionView.translatesAutoresizingMaskIntoConstraints = false;
        peopleCollectionView.layoutIfNeeded()
        progressUtils.showActivityIndicator(uiView: self.view)
        viewModel.getPopularPeople()
    }
    
    func reloadTablePeople(dataArray: [PeopleModel]) {
        self.dataItems.append(contentsOf: dataArray)
        print("Success")
        progressUtils.hideActivityIndicator(uiView: view)
        peopleCollectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        dataItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! PeopleCell
        let url = APIConstants.IMAGE_URL + (dataItems[indexPath.row].profile_path ?? "")
        cell.updateCell(url: url, name: dataItems[indexPath.row].name ?? "--")
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.bounds.size.width)-1, height: 110)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}
