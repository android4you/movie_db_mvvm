//
//  PeopleCell.swift
//  Movie_db_App
//
//  Created by Manu Aravind on 08/04/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import UIKit

class PeopleCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameView: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func updateCell(url: String , name: String){
        imageView.setImageFromURl(stringImageUrl: url)
        nameView.text = name
        imageView.layer.masksToBounds = true
          imageView.layer.cornerRadius = imageView.bounds.width / 2
    }
}
